package com.cw360.shortcuts.dao;

import java.util.List;

import com.cw360.shortcuts.bean.AdminsConfigBean;
import com.google.appengine.api.datastore.Entity;

public class AdminsConfigDao extends DaoInterfaceAbstract<AdminsConfigBean>{

	public void saveAdminsConfig(AdminsConfigBean adminsConfig) {
		saveBean(adminsConfig);
	}

	public List<AdminsConfigBean> listAdminsConfig() {
		return getBeanList(AdminsConfigBean.class, null, null, null);
	}

	@Override
	public AdminsConfigBean buildBean(Entity entity) {
		AdminsConfigBean adminsConfig = new AdminsConfigBean();
		adminsConfig.setId(entity.getKey().getId());
		adminsConfig.setEmails((String) entity.getProperty("emails"));
		adminsConfig.setDateLastModified((Long) entity.getProperty("dateLastModified"));
		adminsConfig.setEmailLastModifier((String) entity.getProperty("emailLastModifier"));
		return adminsConfig;
	}

	@Override
	public Entity buildEntity(AdminsConfigBean object) {
		Entity entity = new Entity(AdminsConfigBean.class.getSimpleName(), object.getId());
		entity.setProperty("emails", object.getEmails());
		entity.setProperty("dateLastModified", object.getDateLastModified());
		entity.setProperty("emailLastModifier", object.getEmailLastModifier());
		return entity;
	}
}
