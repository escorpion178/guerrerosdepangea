package com.cw360.shortcuts.dao;

import java.util.List;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.google.appengine.api.datastore.Entity;

public class ChromeLogoDao extends DaoInterfaceAbstract<ChromeLogo> {
	
	public List<ChromeLogo> getChromeLogos() {
		return getBeanList(ChromeLogo.class, null, null, null);
	}
	
	public ChromeLogo getFirstChromeLogoByDominio(String dominio) {
		return getBean(ChromeLogo.class, dominio);
//		Filter filtroDominio = new FilterPredicate("dominio", FilterOperator.EQUAL, dominio);
//		List<ChromeLogo> chromeLogo = getBeanList(ChromeLogo.class, filtroDominio, null, null);
//		return chromeLogo.isEmpty() ? null : chromeLogo.get(0);
	}
//
//	public ChromeLogo getFirstLogo() {
//		List<ChromeLogo> logos = getBeanList(ChromeLogo.class, null, null, 1);
//		return logos.isEmpty() ? null : logos.get(0);
//	}
//	
	public void deleteLogo(String dominio) {
		deleteBean(ChromeLogo.class, dominio);
	}
	
	public void saveChromeLogo(ChromeLogo chromeLogo) {
		saveBean(chromeLogo);
	}
		
	//es para obtener data
	@Override
	public ChromeLogo buildBean(Entity entity) {
		ChromeLogo clogo = new ChromeLogo();
		//clogo.setLogoId(entity.getKey().getId());
		clogo.setDominio(entity.getKey().getName());
		clogo.setStrBlobKey(DatastoreUtilsDao.getTextString(entity, "strBlobKey"));
		clogo.setTitulo(DatastoreUtilsDao.getTextString(entity, "titulo"));
		clogo.setUrl(DatastoreUtilsDao.getTextString(entity, "url"));
		//clogo.setDominio(DatastoreUtilsDao.getTextString(entity, "dominio"));
		clogo.setGanchoDominio(DatastoreUtilsDao.getTextString(entity, "ganchoDominio"));
		clogo.setGanchoLogo(DatastoreUtilsDao.getTextString(entity, "ganchoLogo"));
		clogo.setUbicacion(DatastoreUtilsDao.getTextString(entity, "ubicacion"));
		return clogo;		
	}
	
	//es para guardar data "setear"
	@Override
	public Entity buildEntity(ChromeLogo object) {
		Entity entity = new Entity(ChromeLogo.class.getSimpleName(), object.getDominio());
		entity.setUnindexedProperty("strBlobKey", object.getStrBlobKey());
		entity.setProperty("titulo", object.getTitulo());		
		entity.setUnindexedProperty("url", object.getUrl());
		//entity.setProperty("dominio", object.getDominio());
		entity.setProperty("ganchoDominio", object.getGanchoDominio());
		entity.setProperty("ganchoLogo", object.getGanchoLogo());
		entity.setProperty("ubicacion", object.getUbicacion());
		return entity;
	}

	
}
