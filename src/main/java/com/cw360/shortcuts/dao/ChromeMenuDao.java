package com.cw360.shortcuts.dao;

import java.util.List;

import com.cw360.shortcuts.bean.ChromeMenu;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;

public class ChromeMenuDao extends DaoInterfaceAbstract<ChromeMenu> {
	
	public ChromeMenu getFirstMenu() {
		List<ChromeMenu> menus = getBeanList(ChromeMenu.class, null, null, 1);
		return menus.isEmpty() ? null : menus.get(0);
	}
	
	public List<ChromeMenu> getListMenu() {
		return getBeanList(ChromeMenu.class, null, null, null);
	}

	public void deleteMenu(Long menuId) {
		deleteBean(ChromeMenu.class, menuId);
	}
	
	public void saveMenu(ChromeMenu chromeMenu) {
		saveBean(chromeMenu);
	}
		
	//es para obtener data
	@Override
	public ChromeMenu buildBean(Entity entity) {
		ChromeMenu menu = new ChromeMenu();
		menu.setMenuId(entity.getKey().getId());
		menu.setGanchoDominio(DatastoreUtilsDao.getTextString(entity, "ganchoDominio"));
		menu.setGanchoLogo(DatastoreUtilsDao.getTextString(entity, "ganchoLogo"));
		menu.setIdspreadsheet(DatastoreUtilsDao.getTextString(entity, "idspreadsheet"));
		menu.setMenuUrlIcono(DatastoreUtilsDao.getTextString(entity, "menuUrlIcono"));
		menu.setMenuUrlIcono(DatastoreUtilsDao.getTextString(entity, "menuLink"));
		menu.setMenuTitle(DatastoreUtilsDao.getTextString(entity, "menuTitle"));
		menu.setMenuPosicion(DatastoreUtilsDao.getTextString(entity, "menuPosicion"));
		menu.setMenuClass(DatastoreUtilsDao.getTextString(entity, "menuClass"));
		menu.setMenuEsAdmin(DatastoreUtilsDao.getTextString(entity, "menuEsAdmin"));
		return menu;		
	}
	
	//es para guardar data "setear"
	@Override
	public Entity buildEntity(ChromeMenu object) {
		Entity entity = new Entity(ChromeMenu.class.getSimpleName(), object.getMenuId());
		entity.setUnindexedProperty("ganchoDominio", getTextOrStringEntity(object.getGanchoDominio()));
		entity.setUnindexedProperty("ganchoLogo", getTextOrStringEntity(object.getGanchoLogo()));
		entity.setUnindexedProperty("idspreadsheet", getTextOrStringEntity(object.getIdspreadsheet()));
		entity.setUnindexedProperty("menuUrlIcono", getTextOrStringEntity(object.getMenuUrlIcono()));
		entity.setUnindexedProperty("menuLink", getTextOrStringEntity(object.getMenuLink()));
		entity.setUnindexedProperty("menuTitle", getTextOrStringEntity(object.getMenuTitle()));
		entity.setUnindexedProperty("menuPosicion", getTextOrStringEntity(object.getMenuPosicion()));
		entity.setUnindexedProperty("menuClass", getTextOrStringEntity(object.getMenuClass()));
		entity.setUnindexedProperty("menuEsAdmin", getTextOrStringEntity(object.getMenuEsAdmin()));
		return entity;
	}
	
	public static Object getTextOrStringEntity(String stringValue) {
		if (stringValue != null && stringValue.length() >= 500) {
			return new Text(stringValue);
		} else {
			return stringValue;
		}
	}
}
