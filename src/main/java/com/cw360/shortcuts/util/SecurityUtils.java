package com.cw360.shortcuts.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.cw360.shortcuts.bean.AdminsConfigBean;
import com.cw360.shortcuts.dao.AdminsConfigDao;
import com.cw360.shortcuts.exception.IsNotAdminException;
import com.cw360.shortcuts.exception.IsNotUserInDomainException;
import com.cw360.shortcuts.exception.NeedToRefreshPageException;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.apphosting.api.ApiProxy;

public class SecurityUtils {

	private static final Logger logger = Logger.getLogger(SecurityUtils.class.getName());

	public static void verifySecurityAdminConfig(HttpServletRequest request) throws IsNotAdminException, NeedToRefreshPageException, IsNotUserInDomainException {

		if (request.getUserPrincipal() == null) {
			try {
				UserService userService = UserServiceFactory.getUserService();
				String loginUrl = userService.createLoginURL(request.getRequestURI());
				throw new NeedToRefreshPageException(loginUrl);
			} catch (IllegalArgumentException iae) {
				throw new IsNotUserInDomainException();
			}
		}
		List<String> listAdminEmails = getListAdminEmails();

		if (listAdminEmails.isEmpty()) {
			// PUEDE ENTRAR CUALQUIERA Y CONFIGURAR LA PRIMERA VEZ.
		} else {
			// se obliga que el usuario logueado sea uno de los admins
			verifySecurity(request, listAdminEmails);
		}
	}

	public static void verifySecurity(HttpServletRequest request) throws IsNotAdminException, NeedToRefreshPageException, IsNotUserInDomainException {
		verifySecurity(request, getListAdminEmails());
	}

	private static void verifySecurity(HttpServletRequest request, List<String> listAdminEmails) throws IsNotAdminException, NeedToRefreshPageException, IsNotUserInDomainException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		try {
			if (user == null) {
				String urlLogin = userService.createLoginURL(request.getRequestURI());
				throw new NeedToRefreshPageException(urlLogin);
			} else {
				listAdminEmails = listAdminEmails == null ? getListAdminEmails() : listAdminEmails;
				String loginEmail = user.getEmail().toLowerCase();
				if (!listAdminEmails.contains(loginEmail)) {
					logger.info(String.format("El user [%s] conectado desde [%s] no tienep permiso.", loginEmail, request.getRemoteAddr()));
					throw new IsNotAdminException(listAdminEmails.toString());
				} else {
					// granted access.
				}
			}
		} catch (IllegalArgumentException iae) {
			logger.log(Level.SEVERE, "Error: IllegalArgumentException", iae);
			throw new IsNotUserInDomainException();
		}
	}

	private static List<String> getListAdminEmails() {
		AdminsConfigDao adminsDao = new AdminsConfigDao();
		List<AdminsConfigBean> listAdminsBeans = adminsDao.listAdminsConfig();
		List<String> listAdminEmails = new ArrayList<String>();
		if (listAdminsBeans.isEmpty()) {
			adminsDao.saveAdminsConfig(new AdminsConfigBean(System.currentTimeMillis(), ""));
		} else {
			for (AdminsConfigBean adminBean : listAdminsBeans) {
				listAdminEmails.addAll(HelperUtils.getListEmails(adminBean.getEmails()));
			}
		}
		return listAdminEmails;
	}

	@SuppressWarnings("unused")
	private static void eviromentVariables() {
		logger.info("domain:" + ApiProxy.getCurrentEnvironment().getAuthDomain() + ", appID:" + ApiProxy.getCurrentEnvironment().getAppId() + ", email:" + ApiProxy.getCurrentEnvironment().getEmail());
		Map<String, Object> map = ApiProxy.getCurrentEnvironment().getAttributes();
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			logger.info("key:" + key + ", value:" + map.get(key));
		}
	}
}
