package com.cw360.shortcuts.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.appengine.repackaged.com.google.common.base.StringUtil;

public class HelperUtils {

	static Logger logger = Logger.getLogger(HelperUtils.class.getName());

	public static void threadSleep(long millis) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.log(Level.SEVERE, "Error InterruptedException", e);
		}
	}

	public static List<String> getListEmails(String strList) {
		return splitStringList(strList, ",; ");
	}

	public static List<String> splitStringList(String strList, String separators) {
		List<String> listAdminEmails = new ArrayList<String>();
		if (strList != null && !strList.trim().isEmpty()) {
			StringTokenizer tok = new StringTokenizer(strList, separators);
			while (tok.hasMoreTokens()) {
				listAdminEmails.add(tok.nextToken().trim().toLowerCase());
			}
		}
		return listAdminEmails;
	}

	public static String getContactId(String urlContactId) {
		String urlContactIdSplitted[] = urlContactId.split("/");
		return urlContactIdSplitted[urlContactIdSplitted.length - 1];
	}

	public static String buildContactMapId(String sourceEmail, String targetEmail, String sourceContactId) {
		return sourceEmail + "_" + targetEmail + "_" + sourceContactId;
	}

	public static String buildReverseContactMapId(String idTarget, String targetEmail) {
		return idTarget + "_" + targetEmail;
	}

	public static String encode(String string) {
		try {
			return URLEncoder.encode(string, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return string;
		}
	}

	public static String decode(String string) {
		try {
			return URLDecoder.decode(string, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return string;
		}
	}
	
	public static String getStringUTF8(String strValue) {
		if (StringUtil.isEmptyOrWhitespace(strValue)) {
			return strValue;
		} else {
			try {
				return new String(strValue.getBytes("UTF-8"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return strValue;
			}
		}
	}
}
