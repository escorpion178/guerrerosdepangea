package com.cw360.shortcuts.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class JsonUtils {

	public static void writeJson(Map<String, Object> map, HttpServletResponse resp) throws IOException {
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		writeJson(map, resp.getWriter());
	}
	public static void writeJson(Object obj, HttpServletResponse resp) throws IOException {
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		writeJson(obj, resp.getWriter());
	}
	
	public static void writeJson(Map<String, Object> map, PrintWriter printWriter) throws IOException {
		Gson gson = new Gson();
		printWriter.println(gson.toJson(map));
		printWriter.flush();
		printWriter.close();
	}
	
	public static void writeJson(Object object, PrintWriter printWriter) throws IOException {
		Gson gson = new Gson();
		printWriter.println(gson.toJson(object));
		printWriter.flush();
		printWriter.close();
	}

	public static String toJsonString(Object object) {
		return new Gson().toJson(object);
	}
	
}
