package com.cw360.shortcuts.exception;

public class IsNotAdminException extends Exception {
	private static final long serialVersionUID = 1L;

	private String emailAddress;

	public IsNotAdminException() {
		super();
	}

	public IsNotAdminException(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public IsNotAdminException(String emailAddress, String message) {
		super(message);
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
