package com.cw360.shortcuts.exception;

public class NeedToRefreshPageException extends Exception {
	private static final long serialVersionUID = 1L;

	private String loginUrl;

	public NeedToRefreshPageException() {
		super();
	}

	public NeedToRefreshPageException(String loginUrl) {
		this.setLoginUrl(loginUrl);
	}

	public NeedToRefreshPageException(String loginUrl, String message) {
		super(message);
		this.setLoginUrl(loginUrl);
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

}
