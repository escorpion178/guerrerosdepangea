package com.cw360.shortcuts.exception;

public class IsNotUserInDomainException extends Exception {
	private static final long serialVersionUID = 1L;

	private String emailAddress;

	public IsNotUserInDomainException() {
		super();
	}

	public IsNotUserInDomainException(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public IsNotUserInDomainException(String emailAddress, String message) {
		super(message);
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
