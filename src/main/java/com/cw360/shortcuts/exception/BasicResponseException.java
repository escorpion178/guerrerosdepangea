package com.cw360.shortcuts.exception;

public class BasicResponseException extends Exception {
	private static final long serialVersionUID = 1L;

	private int errorCode;
	private int errorType;

	public BasicResponseException(int errorType, int errorCode, String message) {
		super(message);
		this.errorType = errorType;
		this.errorCode = errorCode;
	}
	
	public BasicResponseException(int errorType, int errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorType = errorType;
		this.errorCode = errorCode;
	}

	public int getErrorType() {
		return errorType;
	}

	public int getErrorCode() {
		return errorCode;
	}
}
