package com.cw360.shortcuts.bean;

public class JsonResponse {

	private boolean success;
	private Object result;

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
