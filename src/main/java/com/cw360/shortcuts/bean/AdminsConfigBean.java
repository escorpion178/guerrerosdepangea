package com.cw360.shortcuts.bean;

import java.io.Serializable;

public class AdminsConfigBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String emails;
	private String emailLastModifier;
	private Long dateLastModified;

	public AdminsConfigBean() {}

	public AdminsConfigBean(Long id, String emails) {
		this.id = id;
		this.emails = emails;
		this.dateLastModified = System.currentTimeMillis();
		this.emailLastModifier = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public String getEmailLastModifier() {
		return emailLastModifier;
	}

	public void setEmailLastModifier(String emailLastModifier) {
		this.emailLastModifier = emailLastModifier;
	}

	public long getDateLastModified() {
		return dateLastModified;
	}

	public void setDateLastModified(long dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

}