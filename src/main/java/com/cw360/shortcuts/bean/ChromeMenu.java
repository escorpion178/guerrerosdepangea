package com.cw360.shortcuts.bean;

public class ChromeMenu {
	private Long menuId;
	
	private String ganchoDominio; // gancho del dominio (correo)
	private String ganchoLogo; // gancho del logo
	private String idspreadsheet; // id del spreadsheet
	
	private String menuClass;	// class="webinar" --> Es el id a usar para ese item en el spreadsheet
	private String menuTitle;	// titulo de un item del menu
	private String menuUrlIcono;	// Url del icono de un item del menu
	private String menuLink;	// Link de un item del menu	
	
	private String menuPosicion;	// posicion del item en el menu
	private String menuEsAdmin;	// false por defecto

	
	public ChromeMenu() {
		this.menuId = System.currentTimeMillis();
	}
	
	public Long getMenuId() {
		return menuId;
	}
	
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getMenuUrlIcono() {
		return menuUrlIcono;
	}

	public void setMenuUrlIcono(String menuUrlIcono) {
		this.menuUrlIcono = menuUrlIcono;
	}

	public String getMenuTitle() {
		return menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	public String getMenuLink() {
		return menuLink;
	}

	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}

	public String getMenuPosicion() {
		return menuPosicion;
	}

	public void setMenuPosicion(String menuPosicion) {
		this.menuPosicion = menuPosicion;
	}

	public String getMenuClass() {
		return menuClass;
	}

	public void setMenuClass(String menuClass) {
		this.menuClass = menuClass;
	}

	public String getMenuEsAdmin() {
		return menuEsAdmin;
	}

	public void setMenuEsAdmin(String menuEsAdmin) {
		this.menuEsAdmin = menuEsAdmin;
	}

	public String getGanchoDominio() {
		return ganchoDominio;
	}

	public void setGanchoDominio(String ganchoDominio) {
		this.ganchoDominio = ganchoDominio;
	}

	public String getGanchoLogo() {
		return ganchoLogo;
	}

	public void setGanchoLogo(String ganchoLogo) {
		this.ganchoLogo = ganchoLogo;
	}

	public String getIdspreadsheet() {
		return idspreadsheet;
	}

	public void setIdspreadsheet(String idspreadsheet) {
		this.idspreadsheet = idspreadsheet;
	}
	
}



