package com.cw360.shortcuts.bean;

public class ChromeLogo {

	//private Long logoId;
	private String strBlobKey;
	private String titulo;
	private String url;
	
	private String dominio;
	private String ganchoDominio;
	private String ganchoLogo;
	private String ubicacion;

	//public ChromeLogo() {
		//logoId = System.currentTimeMillis();  // TIME_STAMP
	//}

	public String getStrBlobKey() {
		return strBlobKey;
	}

	public void setStrBlobKey(String strBlobKey) {
		this.strBlobKey = strBlobKey;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public String getGanchoDominio() {
		return ganchoDominio;
	}

	public void setGanchoDominio(String ganchoDominio) {
		this.ganchoDominio = ganchoDominio;
	}

	public String getGanchoLogo() {
		return ganchoLogo;
	}

	public void setGanchoLogo(String ganchoLogo) {
		this.ganchoLogo = ganchoLogo;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


}
