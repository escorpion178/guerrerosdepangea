package com.cw360.shortcuts.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.cw360.shortcuts.dao.ChromeLogoDao;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

/**
 * Servlet implementation class serve
 */

public class ServeIconServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		String dominio= req.getParameter("dominio");
		
		ChromeLogoDao logoDao = new ChromeLogoDao();
		ChromeLogo chromeLogo = logoDao.getFirstChromeLogoByDominio(dominio);
		if(chromeLogo!=null){
			BlobKey blobKey = new BlobKey(chromeLogo.getStrBlobKey());
			blobstoreService.serve(blobKey, res);
		}else{
			chromeLogo = logoDao.getFirstChromeLogoByDominio("cloudware360.com");
			if(chromeLogo!=null){
				BlobKey blobKey = new BlobKey(chromeLogo.getStrBlobKey());
				blobstoreService.serve(blobKey, res);
			}else{
				//TODO aqui colocar que devolver si no existe el link por defecto
				
			}			
		}		
	}
}
