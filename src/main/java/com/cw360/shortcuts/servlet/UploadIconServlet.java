package com.cw360.shortcuts.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.cw360.shortcuts.dao.ChromeLogoDao;
import com.cw360.shortcuts.exception.IsNotAdminException;
import com.cw360.shortcuts.exception.IsNotUserInDomainException;
import com.cw360.shortcuts.exception.NeedToRefreshPageException;
import com.cw360.shortcuts.util.SecurityUtils;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

/**
 * Servlet implementation class upload
 */
public class UploadIconServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(UploadIconServlet.class.getName());
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		try {
			SecurityUtils.verifySecurity(req);

			List<BlobKey> blobs = blobstoreService.getUploads(req).get("myFile");
			BlobKey blobKey = blobs.get(0);
			String blobId = "";
			
			String dominio= req.getParameter("dominio");
			logger.info("Dominio es : "+ dominio);
			
			String posicion= req.getParameter("posicion");
			logger.info("posicion : "+ posicion);
			
			if (blobKey == null || dominio==null || dominio.isEmpty()) {
				logger.info("blobKey is null");
			} else {
				blobId = blobKey.getKeyString();
				logger.info("blobId=" + blobId);

				ChromeLogoDao logoDao = new ChromeLogoDao();
				
				ChromeLogo chromeLogo = logoDao.getFirstChromeLogoByDominio(dominio);
				logger.info("DOMINIOoooooooooooo: "+chromeLogo);
				chromeLogo = chromeLogo!=null ? chromeLogo:new ChromeLogo();
				
				logger.info("DOMINIO dentro de entidad=" + chromeLogo.getDominio());
				if (chromeLogo.getStrBlobKey() != null) {
					blobstoreService.delete(new BlobKey(chromeLogo.getStrBlobKey()));
					logger.info("Imagen antigua borrada del blobstore");
				}
				
				chromeLogo.setStrBlobKey(blobId);
				chromeLogo.setTitulo("Shortcut-"+dominio);

				ImagesService imagesService = ImagesServiceFactory.getImagesService();
				String urlLogo = imagesService.getServingUrl(ServingUrlOptions.Builder.withBlobKey(blobKey).secureUrl(true));

				chromeLogo.setUrl(urlLogo);
				chromeLogo.setDominio(dominio);
				chromeLogo.setUbicacion(posicion);
				logoDao.saveChromeLogo(chromeLogo);
				logger.info("imagen guardada");
			}
			res.sendRedirect("/shortcut");
		} catch (NeedToRefreshPageException e) {
			res.sendRedirect(e.getLoginUrl());
			return;
		} catch (IsNotAdminException | IsNotUserInDomainException e) {
			req.getRequestDispatcher("/error_permisos.jsp").forward(req, res);
			return;
		}
	}
}
