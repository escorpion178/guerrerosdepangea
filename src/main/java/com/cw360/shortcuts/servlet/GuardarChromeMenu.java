package com.cw360.shortcuts.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeMenu;
import com.cw360.shortcuts.dao.ChromeMenuDao;
import com.cw360.shortcuts.exception.IsNotAdminException;
import com.cw360.shortcuts.exception.IsNotUserInDomainException;
import com.cw360.shortcuts.exception.NeedToRefreshPageException;
import com.cw360.shortcuts.util.HelperUtils;
import com.cw360.shortcuts.util.SecurityUtils;

/**
 * Servlet implementation class GuardarChromeMenu
 */
public class GuardarChromeMenu extends HttpServlet {
	Logger logger = Logger.getLogger(GuardarChromeMenu.class.getName());
	private static final long serialVersionUID = 1L;
       
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		logger.info("Menu a guardar sin importar logitud2222:");
		
		try {
			SecurityUtils.verifySecurity(req);

			String ganchoDominio = req.getParameter("gancho-dominio");
			String ganchoLogo = req.getParameter("gancho-logo");
			
			//TODO variables
			/*
			  	menuId

				ganchoDominio
				ganchoLogo
				idspreadsheet
				
				menuClass
					menuTitle
				menuUrlIcono
				menuLink
				
				menuPosicion
				menuEsAdmin
			 * */			
//			String idspreadsheet = req.getParameter("id-spreadsheet");
//			String idEsAdmin = req.getParameter("idEsAdmin");
			//String table = req.getParameterValues("table");
//			String htmlMenu = HelperUtils.getStringUTF8(req.getParameter("htmlmenu"));
			
//			ChromeLogo cLogo = new ChromeLogoDao().getFirstLogo();
//			cLogo = cLogo != null ? cLogo : new ChromeLogo();
			//cLogo.setGancho(gancho);
			//cLogo.setSpreadsheet(idspreadsheet);
			//cLogo.setIdEsAdmin(idEsAdmin);
//			new ChromeLogoDao().saveChromeLogo(cLogo);
			
			ChromeMenu cMenu = new ChromeMenuDao().getFirstMenu();			
			cMenu = cMenu != null ? cMenu : new ChromeMenu();
			cMenu.setGanchoDominio(ganchoDominio);
			cMenu.setGanchoLogo(ganchoLogo);
			
//			cMenu.setMenuEsAdmin(idEsAdmin);
//			cMenu.setMenuTitle(htmlMenu);// menu title esta haciendo de variable a guardar el html
			
			new ChromeMenuDao().saveMenu(cMenu);
			res.sendRedirect("/shortcut");
		} catch (NeedToRefreshPageException e) {
			res.sendRedirect(e.getLoginUrl());
			return;
		} catch (IsNotAdminException | IsNotUserInDomainException e) {
			req.getRequestDispatcher("/error_permisos.jsp").forward(req, res);
			return;
		}
	}
}


