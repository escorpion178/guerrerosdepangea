package com.cw360.shortcuts.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.cw360.shortcuts.bean.ChromeMenu;
import com.cw360.shortcuts.dao.ChromeLogoDao;
import com.cw360.shortcuts.dao.ChromeMenuDao;
import com.cw360.shortcuts.exception.IsNotAdminException;
import com.cw360.shortcuts.exception.IsNotUserInDomainException;
import com.cw360.shortcuts.exception.NeedToRefreshPageException;
import com.cw360.shortcuts.util.SecurityUtils;

/**
 * Servlet implementation class PangeaServlet
 */
public class PangeaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		try {
//			SecurityUtils.verifySecurity(request);
			
//			List<ChromeLogo> chromeLogos = new ChromeLogoDao().getChromeLogos();
//			if (chromeLogos == null){
//				//TODO colocar alguna data por defecto al chromeLogo
//			}
//			String strListLogos ="";
//			for(ChromeLogo logo:chromeLogos){
//				strListLogos += "'" + logo.getDominio() + "',";
//			}
//			strListLogos = "[" + (strListLogos.isEmpty() ? "" : strListLogos.substring(0, strListLogos.length() -1)) + "]";
//			
//			ChromeMenu cMenu = new ChromeMenuDao().getFirstMenu();			
//			cMenu = cMenu != null ? cMenu : new ChromeMenu();
//			
//			String ganchoDominio = cMenu.getGanchoDominio() != null ? cMenu.getGanchoDominio() : "";
//			String ganchoLogo = cMenu.getGanchoLogo() != null ? cMenu.getGanchoLogo() : "";
//			
//			request.setAttribute("ganchoDominio", ganchoDominio);
//			request.setAttribute("ganchoLogo", ganchoLogo);
//			request.setAttribute("chromeLogos", chromeLogos);
//			request.setAttribute("strListLogos", strListLogos);
			
			request.getRequestDispatcher("/pangea.jsp").forward(request, response);
//		} catch (NeedToRefreshPageException e) {
//			response.sendRedirect(e.getLoginUrl());
//			return;
//		} catch (IsNotAdminException | IsNotUserInDomainException e) {
//			request.getRequestDispatcher("/error_permisos.jsp").forward(request, response);
//			return;
//		}
	}

}

