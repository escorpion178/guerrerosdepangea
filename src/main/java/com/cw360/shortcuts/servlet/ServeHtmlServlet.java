package com.cw360.shortcuts.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.cw360.shortcuts.bean.ChromeMenu;
import com.cw360.shortcuts.dao.ChromeLogoDao;
import com.cw360.shortcuts.dao.ChromeMenuDao;
import com.cw360.shortcuts.util.HelperUtils;
import com.cw360.shortcuts.util.JsonUtils;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

/**
 * Servlet implementation class ServeHtmlServlet
 */
public class ServeHtmlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ServeHtmlServlet.class.getName());
	//private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
//		String ganchoDominio= req.getParameter("ganchoDominio");
//		String ganchoLogo= req.getParameter("ganchoLogo");
//		String valor = "";
//		if(ganchoDominio.length()!=0 && ganchoDominio!=null){
//			ChromeMenu cMenu = new ChromeMenuDao().getFirstMenu();			
//			cMenu = cMenu != null ? cMenu : new ChromeMenu();			
//			ganchoDominio = cMenu.getGanchoDominio() != null ? cMenu.getGanchoDominio() : "";
//			valor =ganchoDominio;
//		}
//		if(ganchoLogo.length()!=0 && ganchoLogo!=null){
//			ChromeMenu cMenu = new ChromeMenuDao().getFirstMenu();			
//			cMenu = cMenu != null ? cMenu : new ChromeMenu();			
//			ganchoLogo = cMenu.getGanchoLogo() != null ? cMenu.getGanchoLogo() : "";
//			valor =ganchoLogo;
//		}
		String menu= req.getParameter("menu");
		logger.info("Menu es : "+ menu);
		String valor;
		ChromeMenu cMenu = new ChromeMenuDao().getFirstMenu();			
		cMenu = cMenu != null ? cMenu : new ChromeMenu();
		switch(menu){
			case "dominio":menu = cMenu.getGanchoDominio() != null ? cMenu.getGanchoDominio() : "";
						logger.info("llamando al gancho dominio guardado : "+ menu);
						valor =menu;
						break;
			case "logo":menu = cMenu.getGanchoLogo() != null ? cMenu.getGanchoLogo() : "";
						logger.info("llamando al gancho logo guardado : "+ menu);
						valor =menu;
						break;
			default: valor="";
				break;
		}
		
		JsonUtils.writeJson(valor, res);		
	}

}
