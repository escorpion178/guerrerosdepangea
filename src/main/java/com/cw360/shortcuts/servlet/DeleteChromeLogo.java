package com.cw360.shortcuts.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cw360.shortcuts.bean.ChromeLogo;
import com.cw360.shortcuts.dao.ChromeLogoDao;
import com.cw360.shortcuts.exception.IsNotAdminException;
import com.cw360.shortcuts.exception.IsNotUserInDomainException;
import com.cw360.shortcuts.exception.NeedToRefreshPageException;
import com.cw360.shortcuts.util.SecurityUtils;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

/**
 * Servlet implementation class deleteChromeLogo
 */
public class DeleteChromeLogo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(DeleteChromeLogo.class.getName());
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		logger.info("Eliminar logo multidominio");
		
		try {
			SecurityUtils.verifySecurity(req);
			String dominio = req.getParameter("dominio");
			
			ChromeLogoDao logoDao = new ChromeLogoDao();			
			ChromeLogo chromeLogo = logoDao.getFirstChromeLogoByDominio(dominio);
			chromeLogo = chromeLogo!=null ? chromeLogo:new ChromeLogo();
			if (chromeLogo.getStrBlobKey() != null) {			
				blobstoreService.delete(new BlobKey(chromeLogo.getStrBlobKey()));
				logoDao.deleteLogo(dominio);
				logger.info("Imagen antigua borrada del blobstore");
			}
		} catch (NeedToRefreshPageException e) {
			res.sendRedirect(e.getLoginUrl());
			return;
		} catch (IsNotAdminException | IsNotUserInDomainException e) {
			req.getRequestDispatcher("/error_permisos.jsp").forward(req, res);
			return;
		}
	}

}
