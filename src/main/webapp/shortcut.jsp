<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();%>

<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/ico" href="/favicon.ico">
	<title>Shortcut CW360</title>
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<link href="vendor/css/design.css" media="all" rel="stylesheet" type="text/css" />	
	<link href="vendor/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>	
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">		
		function validarNuevo(form) {
			var listDominios = ${strListLogos};
			var dominio = $(form["dominio"]).val();
			var logo = $(form["myFile"]).val();
			if(listDominios.indexOf( $.trim(dominio) ) == -1){
				if(logo.length == 0 || dominio.length == 0){
					alert("Es necesario ingresar un dominio y cargar un logo, para guardar.");
					return false;
				}else{					
					if (!logo.match(/(?:gif|jpg|jpeg|png)$/)) {
					    alert("Solo se permiten los formatos: jpg, jpeg, gif y png");
					    return false;
					}
					return true;
				}			
			}else{
				alert("El dominio que intenta ingresar ya se encuentra registrado.");
				return false;
			}		
		}
		function validarModificacion(form) {			
			var dominio = $(form["dominio"]).val();
			var logo = $(form["myFile"]).val();
			if(logo.length == 0){
				alert("Es necesario cargar un logo para guardar.");
				return false;
			}else{
				if (!logo.match(/(?:gif|jpg|jpeg|png)$/)) {
				    alert("Solo se permiten los formatos: jpg, jpeg, gif y png");
				    return false;
				}
				return true;
			}					
		}
	</script>
	
</head>
<body>
	<h1 ALIGN=center>Logos Multidominio</h1>
	<br />
	<div class="row col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-10 col-md-10 col-lg-10 ">		
		<div id="table" class="table-editable">
			<span class="table-add glyphicon glyphicon-plus"></span>
			<div class="table table-striped table-hover table-condensed">		        
		        <div class="fila" style="font-weight:bold;">
		          <div class="celda a">Id</div>
		          <div class="celda b">Dominio</div>
		          <div class="celda c" style="padding-top:5px;">Logo</div>
		          <div class="celda d"></div>
		          <div class="celda e"></div>
		          <div class="celda f"></div>
		        </div>
		          	<c:forEach var="logochrome" items="${chromeLogos}">
						<div class="fila">
				            <form action="<%=blobstoreService.createUploadUrl("/upload")%>" method="post" enctype="multipart/form-data" onsubmit="return validarModificacion(this)">
				            	<div class="celda a">
				            		<input type="text" name="posicion" class="form-control" value="-" readonly>
				            	</div>
					            <div class="celda b">
					            	<input type="text" name="dominio" placeholder="cloudware360.com" class="form-control" value="${logochrome.dominio}" readonly>
					            </div>
					            <div class="celda c">
					            	<div class="form-group">
										<input type="file" class="test-upload" name="myFile" data-show-remove="false" data-show-preview="false" data-show-upload="false">
										<div id="errorBlock" class="help-block"></div>
									</div>
					            </div>
					            <div class="celda d">
					            	<img style="height:37px;" alt="imagen del logo ${logochrome.dominio}" src="http://logo-multidomio-cw360.appspot.com/serve?dominio=${logochrome.dominio}">
					            </div>
					            <div class="celda e"><button type="submit" class="table-save glyphicon glyphicon-floppy-disk"></button></div>
					            <div class="celda f"><span class="table-remove glyphicon glyphicon-remove"></span></div>
				            </form>
			            </div>
					</c:forEach>  
		            
		            <div class="fila novo">
		            <form action="<%=blobstoreService.createUploadUrl("/upload")%>" method="post" enctype="multipart/form-data" onsubmit="return validarNuevo(this)">
		            	<div class="celda a">
		            		<input type="text" name="posicion" class="form-control" value="-" readonly>
		            	</div>
			            <div class="celda b"><input type="text" name="dominio" placeholder="cloudware360.com" class="form-control"></div>
			            <div class="celda c">
			            	<div class="form-group">
								<input type="file" class="test-upload" name="myFile" data-show-remove="false" data-show-preview="false" data-show-upload="false">
								<div id="errorBlock" class="help-block"></div>
							</div>
			            </div>
			            <div class="celda d"></div>
			            <div class="celda e"><button type="submit" class="table-save glyphicon glyphicon-floppy-disk"></button></div>
			            <div class="celda f"><span class="table-remove glyphicon glyphicon-remove"></span></div>
		            </form>
		            </div>
		            
		            <div class="fila hide novo">
		            <form action="<%=blobstoreService.createUploadUrl("/upload")%>" method="post" enctype="multipart/form-data" onsubmit="return validarNuevo(this)">
		            	<div class="celda a">
		            		<input type="text" name="posicion" class="form-control" value="-" readonly>
		            	</div>
			            <div class="celda b"><input type="text" name="dominio" placeholder="cloudware360.com" class="form-control"></div>
			            <div class="celda c">
			            	<div class="form-group">
								<input type="file" class="test-upload" name="myFile" data-show-remove="false" data-show-preview="false" data-show-upload="false">
								<div id="errorBlock" class="help-block"></div>
							</div>
			            </div>
			            <div class="celda d"></div>
			            <div class="celda e"><button type="submit" class="table-save glyphicon glyphicon-floppy-disk"></button></div>
			            <div class="celda f"><span class="table-remove glyphicon glyphicon-remove"></span></div>
		            </form>
		            </div>
		            
		          
	     	</div>
		</div>      		
		<div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">				
			<br>
			<div  class= "col-xs-6 input-group" > 
			  <span  class= "input-group-addon"> Google dominio</span> 
			  <input id= "gancho-dominio" type= "text"  class= "form-control"  value="${ganchoDominio}" placeholder= "div.gb_ta"  aria-describedby= "basic-addon1" > 
			</div>
			<br>
			<div  class= "col-xs-6 input-group" > 
			  <span  class= "input-group-addon"> Google logo </span> 
			  <input id= "gancho-logo" type= "text"  class= "form-control"  value="${ganchoLogo}" placeholder= ".gb_Wa"  aria-describedby= "basic-addon1" > 
			</div>
			<br>
				<button id="btn_html" onclick="guardarMenu()" type="submit" class="btn btn-lg btn-primary" style="padding-top: 5;margin-top: 20;padding-bottom: 5;margin-bottom: 15;">Guardar</button>											        
		</div>
	</div>
	
</body>
<script>
	var $TABLE = $('#table');
	$('.table-add').click(function () {
	  	var $clone = $TABLE.find('div.hide').clone(true).removeClass('hide');
	  	$TABLE.find('div.table').append($clone);
	});
	$('.table-remove').click(function () {
		if($(this).parents('.novo').length !=0){
			$(this).parents('.fila').remove();
		}else{
			if(confirm("¿Esta seguro de eliminar este dominio?")){
				var $padre = $(this).parents('.fila');
			  	var dominio= $padre.find('input[name="dominio"]').val();
				$padre.remove();// llama a su padre fila
				if(dominio!=null && dominio!=""){				
					logoDelete(dominio);
				}
			}
		}				
	});
	
	function logoDelete(dominio){
		var datajax ={
	    		"dominio" : dominio	    		
	   	    };
		
	   	    $.ajax({
	   	        url : 'deleteChromeLogo',
	   	        type : 'POST',
	   	        dataType : "json",
	   	        data : datajax   	         	        
	   	    });
	}
	
</script>

<script>
	function guardarMenu(){
		
		var datajax ={
    		"gancho-dominio" : $("#gancho-dominio").val(),
    		"gancho-logo" : $("#gancho-logo").val()
	   	};
    	previoMenu();   		
   	    $.ajax({
   	        url : 'guardarChromeMenu',
   	        type : 'POST',
   	        dataType : "json",
   	        data : datajax   	         	        
   	    });
    }
	function previoMenu(){
		$("#design").empty();
   	 	var str = $("#textareahtml").val();
   	 	//TODO "jslint" valida errores como las comas, punto y coma.etc.
   		   
   		$("#design").prepend(str);
	}
</script>

</html>