// 
// *********************** VARIABLES GLOBALES *******************************************
var email = "";
ganchoLogo();
ganchoDominio();
var googlediv = "";// Define la clase utilizada para amarrar el boton y/o botones
var googledomi = "";
// *********************** ACCIONES *******************************************
// Arranca el sistema
$(document).ready(function() {
    $.ajaxSetup({ cache: false });
    checkDOMChange();
});
// Inicia el sistema
function inicializa(){
    cargarBotonUsuario();    
}

// Cargar el botom inicial de usuario
function cargarBotonUsuario(){
    $(googlediv).empty();
    var urlimg = "https://logo-multidomio-cw360.appspot.com/serve"+"?dominio="+obtenerDominio();// Enlace que asigna valor a la imagen usuario. //chrome.extension.getURL('img/icon24.png');
    $(googlediv).prepend('<div id ="boton_usuario">'+
                            '<div id="logo_usuario">'+                
                                '<img id="id_logo_usuario" class="icon_menu_rapido" style="padding-top: 5px;max-width:144px;max-height: 55px;" src="'+urlimg+'"/> '+
                            '</div>'+
                        '</div>'
     ); 
    
}

// *********************** FUNCIONES *******************************************
// Refresca si es que no existe una clase de donde amarrarse.S
function checkDOMChange()
{
    if ($(googlediv).length == 0 || $(googledomi).length == 0) {
        setTimeout(checkDOMChange, 100);
    } else {
        cargarBotonUsuario();
    }
}
function obtenerDominio(){
    var ganchodominio=googledomi;
    var dominio = $(ganchodominio).text();
    dominio= $.trim(dominio);
    dominio= dominio.substring(dominio.indexOf('@')+1,dominio.length);
    return dominio;
}

// Devuelve el valor de la clase de donde se amarra el botom.
function ganchoLogo(){//TODO Que importe su valor desde appengine
    var enlaceLogo = "https://logo-multidomio-cw360.appspot.com/serveHtml?menu=logo";// Enlace que asigna valor al html.
    
    try{
       $.getJSON(enlaceLogo,function(result){
           googlediv = result;        
      });
    }catch (e) {
        alert("El error es: "+e);
    }
}
function ganchoDominio(){
    var enlaceDominio = "https://logo-multidomio-cw360.appspot.com/serveHtml?menu=dominio";// Enlace que asigna valor al html.
    
    try{
       $.getJSON(enlaceDominio,function(result){
            googledomi = result;       
      });
    }catch (e) {
        alert("El error es: "+e);
    }
}